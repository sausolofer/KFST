/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kfst.test;

import KFST.dataset.DatasetInfo;
import KFST.featureSelection.filter.supervised.InformationGain;
import KFST.featureSelection.filter.unsupervised.LaplacianScore;
import KFST.featureSelection.filter.unsupervised.UFSACO;
import KFST.featureSelection.filter.unsupervised.MGSACO;

/**
 *
 * @author SSF
 */
public class Test {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    public static void main(String[] args) {
        //reading the datasets files
        DatasetInfo data = new DatasetInfo();
        data.preProcessing("data/Wine_data.csv", "data/Wine_class.label");
        //printing some information of the dataset
        System.out.println(" no. of all samples : " + data.getNumData()
                + "\n no. of samples in training set : " + data.getNumTrainSet()
                + "\n no .of samples in test set : " + data.getNumTestSet()
                + "\n no. of features : " + data.getNumFeature()
                + "\n no. of classes : " + data.getNumClass());
        //performing the feature selection by information gain method
        //InformationGain method = new InformationGain(3);
        //LaplacianScore method = new LaplacianScore(3, 1, 5);
        //UFSACO method = new UFSACO(5, 0.2, 50, data.getNumFeature(), 5, 0.2, 1.0, 0.7);
        MGSACO method = new MGSACO(5, 0.2, 50, data.getNumFeature(),0.2, 1.0, 0.7);
        method.loadDataSet(data);
        method.evaluateFeatures();
        int[] subset = method.getSelectedFeatureSubset();
        double[] infoGainValues = method.getValues();
        //printing the subset of selected features
        System.out.print("\n subset of selected features: ");
        for (int i = 0; i < subset.length; i++) {
            System.out.print((subset[i] + 1) + " ");
        }
        //printing the information gain values
        if (infoGainValues != null) {
            System.out.println("\n\n information gain values: ");
            for (int i = 0; i < infoGainValues.length; i++) {
                System.out.println(" " + (i + 1) + " : " + infoGainValues[i]);
            }
        }

        //creating reduced datasets as the CSV file format
        data.createCSVFile(data.getTrainSet(), subset, "data/newTrainSet.csv");
        data.createCSVFile(data.getTestSet(), subset, "data/newTestSet.csv");
    }
}
